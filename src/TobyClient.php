<?php
/**
 * Created by PhpStorm.
 * User: zhoulongtao
 * Date: 2021-03-03
 * Time: 14:36
 */

namespace toby;

use toby\signData\Des;
use toby\signData\Rsa;

class TobyClient
{

    /**
     * 渠道
     * @var string
     */
    public $channel;

    /**
     *
     * md5 key 签名
     * @var
     */

    public $md5Key;
    /**
     * 私钥文件路径
     * @var
     */
    public $rsaPrivateKeyFilePath;

    /**
     * 3des key
     * @var string
     */
    public $desKey = '';

    /**
     * 私钥值
     * @var
     */
    public $rsaPrivateKey;

    /**
     * 网关
     * @var string
     */
    public $gatewayUrl;
    /**
     * 返回数据格式
     * @var string
     */
    public $format = "json";
    /**
     * api版本 default 默认 v1 版本1 v2 版本2
     * @var string
     */
    public $apiVersion = "default";

    /**
     * 使用文件读取文件格式，请只传递该值
     * @var null
     */
    public $tobyPublicKey = null;

    /**
     * 使用读取字符串格式，请只传递该值
     * @var
     */
    public $tobyRsaPublicKey;

    /**
     * 文件字符集
     * @var string
     */
    private $fileCharset = "UTF-8";

    /**
     * 表单提交字符集编码
     * @var string
     */
    public $postCharset = "UTF-8";
    /**
     * sdk 版本
     * @var string
     */
    protected $tobySdkVersion = "toby-sdk-php-20210315";

    public $signType = 'md5';

    //此方法对value做urlencode
    public function getSignContentUrlencode($params)
    {
        ksort($params);

        $stringToBeSigned = "";
        $i = 0;
        foreach ($params as $k => $v) {
            if (false === $this->checkEmpty($v) && "@" != substr($v, 0, 1)) {

                // 转换成目标字符集
                $v = $this->characet($v, $this->postCharset);

                if ($i == 0) {
                    $stringToBeSigned .= "$k" . "=" . urlencode($v);
                } else {
                    $stringToBeSigned .= "&" . "$k" . "=" . urlencode($v);
                }
                $i++;
            }
        }

        unset($k, $v);
        return $stringToBeSigned;
    }

    /**
     * 校验$value是否非空
     *  if not set ,return true;
     *    if is null , return true;
     **/
    protected function checkEmpty($value)
    {
        if (empty($value)) {
            return true;
        }
        return false;
    }

    /**
     * 转换字符集编码
     * @param $data
     * @param $targetCharset
     * @return string
     */
    protected function characet($data, $targetCharset)
    {

        if (!empty($data)) {
            $fileType = $this->fileCharset;
            if (strcasecmp($fileType, $targetCharset) != 0) {
                $data = mb_convert_encoding($data, $targetCharset, $fileType);
            }
        }
        return $data;
    }

    /**
     * 执行
     * @param $request
     * @return bool|string
     * @throws \Exception
     */
    public function execute($request)
    {
        $this->setupCharsets($request);
        /**
         *  如果两者编码不一致，会出现签名验签或者乱码
         */
        if (strcasecmp($this->fileCharset, $this->postCharset)) {

            /**
             * writeLog("本地文件字符集编码与表单提交编码不一致，请务必设置成一样，属性名分别为postCharset!");
             */
            throw new \Exception("文件编码：[" . $this->fileCharset . "] 与表单提交编码：[" . $this->postCharset . "]两者不一致!");
        }
        $iv = $this->apiVersion;
        $request->setApiVersion($iv);
        if ($iv == 'default') {
            try {
                $iv = file_get_contents($this->gatewayUrl . '/api/config/version');
            } catch (\Exception $exception) {
                throw new \Exception('获取通宝系统版本号失败');
            }
        }
        $this->apiVersion = $iv;
        $sysParams = [];
        switch ($iv) {
            case 'v1':
                break;
            case "v2":
                $sysParams['channel'] = $this->channel;
                $sysParams['signType'] = $this->signType;
                $sysParams["timeStamp"] = time();
                $sysParams["nonceStr"] = $this->nonceStr(32);
                break;
            default:
                throw new \Exception('版本号不存在');
                break;
        }
        //获取业务参数
        $apiParams = $request->getApiParas();

        if ($this->checkEmpty($apiParams['biz_content'])) {
            throw new \Exception(" api request Fail! The reason : encrypt request is not supperted!");
        }

        $data = array_merge($apiParams['biz_content'], $sysParams);

        $data = $this->generateSign($data, $this->signType, $request);

        //发起HTTP请求
        $requestUrl = $this->gatewayUrl . '/' . $request->getApiMethodName();
        $resp = $this->curl($requestUrl, $data);
        return $resp;

    }

    private function setupCharsets($request)
    {
        if ($this->checkEmpty($this->postCharset)) {
            $this->postCharset = 'UTF-8';
        }
        $str = preg_match('/[\x80-\xff]/', $this->channel) ? $this->channel : print_r($request, true);
        $this->fileCharset = mb_detect_encoding($str, "UTF-8, GBK") == 'UTF-8' ? 'UTF-8' : 'GBK';
    }

    private function nonceStr($length)
    {
        $key = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLOMNOPQRSTUVWXYZ';
        for ($i = 0; $i < $length; $i++) {
            $key .= $pattern{mt_rand(0, 35)}; //生成php随机数
        }
        return $key;
    }

    /**
     * 加密
     * @param $param
     * @param $signType
     */
    private function generateSign($param, $signType, $request)
    {
        $this->rsa = new Rsa($this->tobyPublicKey);
        $this->des = new Des($this->desKey);
        $sign = '';
        switch ($this->apiVersion) {
            //版本1只有md5加密
            case 'v1':
                $sort = $request->getSort() == '' ? [] : explode('.', $request->getSort());
                $string = $this->md5Key;
                foreach ($sort as $field) {
                    if (isset($param[$field])) {
                        $string .= $param[$field];
                    } else {
                        throw new \Exception($field . "缺失");
                    }
                }
                $sign = md5($string);
                $param['key'] = $sign;
                return ['datami' => $this->des->encrypt(json_encode($param))];
                break;
            //版本2有 rsa和md5区别
            case "v2":
                ksort($param);
                $signType = strtolower($signType);
                $string = urldecode(http_build_query($param));
                switch ($signType) {
                    case "md5":
                        $param['sign'] = md5($string . "&key=" . $this->md5Key);
                        break;
                    case "rsa";
                        $param['sign'] = $this->rsa->pubEncrypt($string);
                        break;
                }
                return $param;
                break;
            default:
                throw new \Exception('版本号不存在');
                break;
        }
        return $sign;
    }

    protected function curl($url, $postFields = null)
    {
        if ($this->apiVersion == 'v1') {
            $data = json_encode($postFields);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //禁用SSL
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER,
                array('Content-type:application/json', 'Content-Length: ' . strlen($data)));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $output = curl_exec($ch);
            if (curl_errno($ch)) {
                throw new \Exception(curl_error($ch), 0);
            } else {
                $httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if (200 !== $httpStatusCode) {
                    throw new \Exception($output, $httpStatusCode);
                }
            }
            curl_close($ch);
            return $output;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $postBodyString = "";
        $encodeArray = array();
        $postMultipart = false;

        if (is_array($postFields) && 0 < count($postFields)) {

            foreach ($postFields as $k => $v) {
                if ("@" != substr($v, 0, 1)) //判断是不是文件上传
                {

                    $postBodyString .= "$k=" . urlencode($this->characet($v, $this->postCharset)) . "&";
                    $encodeArray[$k] = $this->characet($v, $this->postCharset);
                } else //文件上传用multipart/form-data，否则用www-form-urlencoded
                {
                    $postMultipart = true;
                    $encodeArray[$k] = new \CURLFile(substr($v, 1));
                }

            }
            unset($k, $v);
            curl_setopt($ch, CURLOPT_POST, true);
            if ($postMultipart) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $encodeArray);
            } else {
                curl_setopt($ch, CURLOPT_POSTFIELDS, substr($postBodyString, 0, -1));
            }
        }

        if ($postMultipart) {

            $headers = array('content-type: multipart/form-data;charset=' . $this->postCharset . ';boundary=' . $this->getMillisecond());
        } else {

            $headers = array('content-type: application/x-www-form-urlencoded;charset=' . $this->postCharset);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $reponse = curl_exec($ch);

        if (curl_errno($ch)) {

            throw new \Exception(curl_error($ch), 0);
        } else {
            $httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if (200 !== $httpStatusCode) {
                throw new \Exception($reponse, $httpStatusCode);
            }
        }
        curl_close($ch);
        return $reponse;
    }

}
