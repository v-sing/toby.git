<?php
  
  
  namespace toby\request;
  
  
  use toby\request\extend\RequestConfig;
  use toby\request\interfaces\Request;

  class TobyGetWebAppCoinConsumeRequest extends RequestConfig implements Request
  {
    //获取通宝消费数据
    /**
     * @var array 版本路径列表
     */
    protected $methodNameList = [
      'default' => 'fortune/getWebAppCoinConsumeOrder',
      'v1' => 'v1/fortune/getWebAppCoinConsumeOrder',
      'v2' => 'v2/fortune/getWebAppCoinConsumeOrder',
    ];
    protected $sort = '';
    
  }
