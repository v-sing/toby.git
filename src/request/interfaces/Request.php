<?php

namespace toby\request\interfaces;

/**
 * 模版接口
 *
 * @Author zhoulongtao email1946367301
 * @DateTime 2021-03-04
 */
interface Request
{

    public function setBizContent($bizContent);
    public function getBizContent();
    public function getApiMethodName();
    public function getApiParas();
    public function setApiVersion($apiVersion);
    public function getApiVersion();
    public function getSort();
}
